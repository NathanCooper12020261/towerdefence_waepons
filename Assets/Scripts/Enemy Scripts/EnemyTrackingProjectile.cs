﻿using UnityEngine;
using System.Collections;

public class EnemyTrackingProjectile : BaseProjectile
{

    Vector3 mdirection;
    bool mfired;
    GameObject mlauncher;
    GameObject mtarget;
    public int mdamage;

    // Update is called once per frame
    void Update()
    {
        if (mtarget)
        {
            transform.position = Vector3.MoveTowards(transform.position, mtarget.transform.position, speed * Time.deltaTime);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public override void FireProjectile(GameObject launcher, GameObject target, int damage, float attackSpeed)
    {
        if (target)
        {
            mdirection = (target.transform.position - launcher.transform.position).normalized;
            mfired = true;
            mlauncher = launcher;
            mtarget = target;
            mdamage = damage;
        }
    }
    //TODO: figure out if this section should be a 	[Command]...
    void OnTriggerEnter(Collider other)
    {
        print("zzzzzzzzzz");
        if (other.tag == "Player")
        {
            print("xxxxxxxxxx");
            other.gameObject.GetComponent<Player>().TakeDamage(mdamage);
            Destroy(gameObject);
            //other.gameObject.GetComponent<Enemy>().TakeDamage(mdamage);
            //Destroy(gameObject);
        }

    }
}
