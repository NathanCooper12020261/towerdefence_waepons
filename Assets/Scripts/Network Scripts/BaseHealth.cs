﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class BaseHealth : NetworkBehaviour {
    [SerializeField]
    protected float _maxHealth;
    public float maxHealth {
        get { return _maxHealth; }
        set { _maxHealth = value; }
    }
    [SyncVar]
    protected float _currentHealth;
    public float currentHealth {
        get { return _currentHealth; }
        set { _currentHealth = value; }
    }

    void Awake() {
        Setup();
    }

    public virtual void Setup() {
        currentHealth = maxHealth;
    }
    public virtual void TakeDamage(float _amount) {
        if (!isServer) return;

        currentHealth -= _amount;

        if (currentHealth <= 0) {
            print("Die");
            Die();
        }
        
        //RpcTakeDamage(_amount);

        //print(gameObject.name + " takes: " + _amount + " damage. Now has: " + currentHealth + " health left");
    }
    //public virtual void RpcTakeDamage(float _amount) {
    //    currentHealth -= _amount;
    //}
    public virtual void HealDamage(float _amount) {
        if (!isServer) return;

        currentHealth = Mathf.Min(currentHealth + _amount, maxHealth);
        
        //RpcHealDamage(_amount);
    }
    //[ClientRpc]
    //public virtual void RpcHealDamage(float _amount) {
    //    currentHealth = Mathf.Min(currentHealth + _amount, maxHealth);
    //}


    protected virtual void Die() {
        //TODO: determine if I can leave the defaul die inside this class
        NetworkServer.Destroy(gameObject);
        Destroy(gameObject);
    }

}

