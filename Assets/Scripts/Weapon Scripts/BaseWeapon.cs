﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
/*
 * standard public class that does not derive from mono 
 * since it does no come from mono - it needs to be serializable so 
 * unity knows how to save and use the class in the inspector
*/
[System.Serializable]
public class BaseWeapon : MonoBehaviour {

    public int gunID;

    public bool isFiring = false;
	public bool hasFired = false;
    public string type = "Handgun";
    public int damage = 10;
    public float maxRange = 20f;
    public float fireRate = 0f; //0 is for single fire wepaon, higher than that it will fire many shots. 

    protected bool isADS = false;
	public bool canFire;

    public int currentAmmo;
    public int currentExtraAmmo;
    public int maxClipSize;
    public int maxExtraAmmo;
	public int recoilAmount;
    //int internalAmmo;
    [SerializeField]
	public GameObject AmmoDisplay;//Text AmmoDisplay;

    protected PlayerShoot playerShoot;

    //protected Text AmmoText;

    public Animation gunAnim;
    public AudioSource gunSound;

    public GameObject graphics;

	public GameObject player; 

    public ParticleSystem muzzleFlash;
    void Awake()
    {
        GameObject[] gos = GameObject.FindObjectsOfType(typeof(GameObject)) as GameObject[]; //will return an array of all GameObjects in the scene
        foreach (GameObject go in gos)
        {
            if (go.layer == 8 && go.CompareTag("Player"))//go.layer=="LocalPlayer" && go.CompareTag("Player"))
            {
                player = go;
            }
        }
        playerShoot = transform.root.GetComponent<PlayerShoot>();
        print("This is awake gone" + playerShoot);
    }
    void OnStartClient()
    {
        player = transform.root.gameObject;
    }
    void Start(){
        player = transform.root.gameObject;
		gunAnim = GetComponent<Animation> ();
		playerShoot =  transform.root.GetComponent<PlayerShoot>();//GetComponentInParent<PlayerShoot> ();
		gunSound = GetComponent<AudioSource> ();
		print("This is stqarrt gone"+ playerShoot);


        //TEMP code to get the player for base weapon 
        GameObject[] gos = GameObject.FindObjectsOfType(typeof(GameObject)) as GameObject[]; //will return an array of all GameObjects in the scene
        foreach (GameObject go in gos)
        {
            if (go.layer == 8 && go.CompareTag("Player"))//go.layer=="LocalPlayer" && go.CompareTag("Player"))
            {
                player = go;
            }
        }


        /* Debug.Log ("Trying ...");
           Text[]texts = AmmoDisplay.GetComponentsInChildren<Text>();
           Debug.Log ("Trying ... the arry is" + texts.Length);
           foreach (Text text in texts) {
               Debug.Log ("Trying to find that panel ");
               if (text.name == "ExtraAmmo") {
                   Debug.Log ("Yup found it");
                   AmmoText = text.GetComponent<Text>();
               }
           }

           }*/
    }


    /*
	public BaseWeapon(Player player)
	{
		this.player = player; 
	}*/

    public bool addAmmo (int ammount)
	{
		if (currentExtraAmmo < maxExtraAmmo) {
			currentExtraAmmo += ammount;
			if (currentExtraAmmo > maxExtraAmmo) {
				currentExtraAmmo = maxExtraAmmo;
			}
			return true; //means ammo was used 
		}
		return false; 
	}


	void Update () {
		//Debug.Log (AmmoText);
		//AmmoText.GetComponent<Text>().text = "" + currentAmmo + "/ " + currentExtraAmmo;
		/*
		if (playerShoot == null) {
			playerShoot = transform.root.GetComponent<PlayerShoot>();
		}
		print("common not null "+ playerShoot);*/

	}

	protected void UpdateAmmo(){
		AmmoDisplay = player.GetComponent<Player> ().UI.ammoUI;//.txtAmmo;
		AmmoDisplay.GetComponent<AmmoPanelScript>().UpdateAmmoText(currentAmmo, currentExtraAmmo);
	}

	protected void UpdateAmmo(string text, string extraText){
		//AmmoDisplay = GameObject.FindGameObjectWithTag ("AmmoPanel");
		AmmoDisplay = player.GetComponent<Player>().UI.ammoUI;//.txtAmmo;
		AmmoDisplay.GetComponent<AmmoPanelScript> ().UpdateAmmoText (text, extraText);//"Zak");//text);
	}

}

