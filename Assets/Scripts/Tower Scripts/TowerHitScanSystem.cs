﻿using UnityEngine;
using System.Collections;

public class TowerHitScanSystem : MonoBehaviour
{
	public ParticleSystem muzzleFlash;

	public float attackRate;
	public float damage;
	public float dps;
    public float fireTimer;
    float attacksPerSecond;

	public GameObject target;
	public float fieldOfView;

	// Use this for initialization
	void Start()
	{
	}

	// Update is called once per frame
	void Update()
	{
		attacksPerSecond = 1 / attackRate;
		dps = (float)damage * attacksPerSecond;
        if (gameObject.GetComponent<TowerTracking>().target == null) return;
		target = gameObject.GetComponent<TowerTracking>().target;
        fireTimer += Time.deltaTime;
        if (gameObject.GetComponent<TowerTracking>().trackCount > 0)
		{
			{
				//float angle = Quaternion.Angle(transform.rotation, Quaternion.LookRotation(target.transform.position - transform.position));
				float angle = Quaternion.Angle(transform.FindChild("Pivot").rotation, Quaternion.LookRotation(target.transform.position - transform.position));

				if (angle < fieldOfView && fireTimer >= attackRate)
				{
                    fireTimer = 0;
					muzzleFlash.Play ();
					this.gameObject.GetComponent<AudioSource> ().Play ();
					target.gameObject.GetComponent<Enemy>().TakeDamage(damage);

				}

			}
		}
	}
}