﻿using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(Player))]
public class PlayerSetip : NetworkBehaviour {

	[SerializeField] //this is so the player UI elements are attached to indiv players
	GameObject playerUIPrefab;
	private GameObject playerUIInstance;

	private Player player;
	//list of compontents you want to disable 
	[SerializeField] //lets you see it ininspector 
	public Behaviour[] componentsToDisable;

	[SerializeField]
	string remoteLayerName = "RemotePlayer";

	Camera sceneCamera;

	[SerializeField]
	string dontDrawLayerName = "PlayerModel";

	[SerializeField]
	GameObject playerGraphics;


	void Start()
	{
		//check if on network
		if (!isLocalPlayer) {
			DisableComponents ();
			AssignRemoteLayer ();			
		} else {
			sceneCamera = Camera.main; 

			//this is so if we dont find the camera we dont get any errors. 
			if (sceneCamera != null) {
				//disables the main camera for the scene for the local player. 
				Camera.main.gameObject.SetActive (false);
			}

			// this will ensure that the local player does not draw their own model 
			SetLayerRecursively(playerGraphics, LayerMask.NameToLayer(dontDrawLayerName));

			//create the playerUI
			playerUIInstance = Instantiate(playerUIPrefab); 
			playerUIInstance.name = "PlayerUI " + netId; //gets rid of the clone part
			playerUIInstance.GetComponent<PlayerUI>().SetPlayer(gameObject);

			//print ("is this nuyll?" + gameObject); 
			//print ("is this 2?" + playerUIInstance); 
			//print ("is this player?" + player); 
			player.Setup (playerUIInstance);

			Debug.Log("Start complete for player"); 
		}
	}

	void SetLayerRecursively (GameObject obj, int newLayer)
	{

		obj.layer = newLayer;

		foreach (Transform child in obj.transform)
		{
			SetLayerRecursively(child.gameObject, newLayer);
		}
	}

	//This is called when a client joins the game 
	public override void OnStartClient()
	{
		base.OnStartClient (); //calls the base functions
		//had to put this here as this seemed to be called before start which is where it was before - Riordan
		player = GetComponent<Player> ();
		//we know we will get the network id since it extends from network behaviour 
		string _netID = GetComponent<NetworkIdentity> ().netId.ToString(); 
		//in order to ensure we always get the player the script must require componenet of player
		GameManager.RegisterPlayer (_netID, player);
	}


	void AssignRemoteLayer()
	{
		gameObject.layer = LayerMask.NameToLayer (remoteLayerName);
	}

	void DisableComponents()
	{
		//go through list of all the components you want to get rid of
		for (int i = 0; i < componentsToDisable.Length; i++) {
			//disable here 
			componentsToDisable [i].enabled = false; 
		}		
	}

	void OnDisable()
	{

		//in clean up we should also desotry the old player UI
		Destroy(playerUIInstance); 

		if (sceneCamera != null) {
			//reenable the camera if you are disconnecting from level 
			sceneCamera.gameObject.SetActive (true);
		}

		//deRegister players once they are killed/disconnected
		GameManager.DeRegisterPlayer(transform.name);
	}
}
