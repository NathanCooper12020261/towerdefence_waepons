﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Characters.FirstPerson;

[RequireComponent(typeof(RectTransform))]
[RequireComponent(typeof(Text))]
public class PlayerUI : MonoBehaviour {
    [SerializeField]
    RectTransform playerHealthBarFill;
    [SerializeField]
    RectTransform baseHealthBarFill;
    [SerializeField]
	public Text txtFunds;
    [SerializeField]
    public Text txtAmmo;
    [SerializeField]
    public Text txtPhase;
    [SerializeField]
    Text txtGameOver;
    [SerializeField]
    Text txtTowerType;
    [SerializeField]
    Text txtTowerPrice;

    public bool showShop;
	public bool showTower;
	private bool tutorialUnlocked;

	private bool isFirstWave = true;

    public GameObject shopUI;
    public GameObject ammoUI;
    public GameObject gameOverUI;
	public GameObject towerUI;
	public GameObject tutorialUI;

	public GameObject trigun, electric, plasma;

	public RawImage damage;
	public RawImage playerDeath;

    Base b;
    GridInteraction gi;
    private GameObject player;
    private Player parentPlayer;
	private WeaponManger weaponmanger; 

    public void SetPlayer(GameObject value) {
        gi = value.GetComponent<GridInteraction>();
        player = value;
        parentPlayer = value.GetComponent<Player>();
        showShop = false;

		weaponmanger = parentPlayer.GetComponent<WeaponManger> ();
    }

    void Awake() {
        b = GameObject.FindGameObjectWithTag("Base").GetComponent<Base>();

		damage.color = new Color (damage.color.r, damage.color.g, damage.color.g, 0);

		//PLAYER DEATH SCREEN - NEED TO WORK OUT HOW TO CROSSFADE
		playerDeath.color = new Color (playerDeath.color.r, playerDeath.color.g, playerDeath.color.g, 0);
    }

    void Update() {

		if (GameManager.singleton.state != GameManager.GameState.LOST) {

			//Fade In playerDeath screen
			if (player.GetComponent<Player> ().isDead) {
				playerDeath.color = new Color (playerDeath.color.r, playerDeath.color.g, playerDeath.color.g, playerDeath.color.a + 0.01f);
			} else {
				playerDeath.color = new Color (playerDeath.color.r, playerDeath.color.g, playerDeath.color.g, 0);
			}


            SetBaseHealthBar(b.currentHealth / b.maxHealth);
            SetPhaseTxt(GameManager.singleton.state, (int)GameManager.singleton.nextPhaseCountdown);
            SetPlayerFundsTxt(player.GetComponent<Player>().funds);
            SetPlayerHealthBar(parentPlayer.currentHealth / parentPlayer.maxHealth);
            if (gi != null) {
                Tower tower = gi.currentTower;
                if (tower != null) {
                    SetTowerTxt(tower.price, tower.type);

					switch (tower.type) {
					case Tower.ELECTRIC:
						{
							ShowTower (electric);
							break;
						}
					case Tower.PLASMA:
						{
							ShowTower (plasma);
							break;
						}
					case Tower.TRIGUN:
						{
							ShowTower (trigun);
							break;
						}
					}
                }
            }

			//Update Damage redness to player health
			damage.color = new Color (damage.color.r, damage.color.g, damage.color.g, (player.GetComponent<Player>().maxHealth - player.GetComponent<Player>().currentHealth)/100);


            //Show the Shop UI and TowerUI.
            if (GameManager.singleton.state == GameManager.GameState.PHASE_BUILDING) {
				if (Input.GetKeyDown (KeyCode.Return)) {
					isFirstWave = false;
				}


				if (player.GetComponent<GridInteraction> ().gridInteractionEnabled) {
					showTower = true;
				} else {
					showTower = false;
				}


                if (Input.GetKeyDown(KeyCode.Tab)) {
                    showShop = !showShop;
                }
            }

            //Turn off shop if fighting
            if (GameManager.singleton.state == GameManager.GameState.PHASE_FIGHTING) {
                showShop = false;

            }

			if (showTower) {
				towerUI.SetActive(true);
			} else {
				towerUI.SetActive(false);
			}


			//Tutorial UI - stops mouse from freezing
			if (GameObject.FindGameObjectWithTag("Tutorial") != null) {
				if (GameObject.FindGameObjectWithTag("Tutorial").GetComponent<TutorialScript> ().isShowingUI) {

					Cursor.lockState = CursorLockMode.None;
					player.GetComponent<PlayerMovement> ().mouseLock = true;
					tutorialUnlocked = true;
					player.GetComponent<WeaponManger> ().CanFire (false);


				} else {
					if (!showShop) {
						Cursor.lockState = CursorLockMode.Locked;
						player.GetComponent<PlayerMovement> ().mouseLock = false;
						tutorialUnlocked = false;
						player.GetComponent<WeaponManger> ().CanFire (false);
					}
				}
			}





			//Show Shop

            if (showShop) {
                shopUI.SetActive(true);
                Cursor.lockState = CursorLockMode.None;
				player.GetComponent<PlayerMovement> ().mouseLock = true;
				player.GetComponent<WeaponManger> ().CanFire (false);//.canFire = false;
            }
            else {
                shopUI.SetActive(false);
				if (GameObject.FindGameObjectWithTag ("Tutorial") != null) {
					if (!tutorialUnlocked) {
						Cursor.lockState = CursorLockMode.Locked;
						player.GetComponent<PlayerMovement> ().mouseLock = false;
						player.GetComponent<WeaponManger> ().CanFire (true);
					}
				} else {
					Cursor.lockState = CursorLockMode.Locked;
					player.GetComponent<PlayerMovement> ().mouseLock = false;
					player.GetComponent<WeaponManger> ().CanFire (true);
				}

            }





			if (weaponmanger.isActive)//((WeaponManger)FindObjectOfType(typeof(WeaponManger))).isActive) {
			{
				ammoUI.SetActive(true);
            } else {
                ammoUI.SetActive(false);
            }
        } else {
            if (shopUI.activeInHierarchy) {
                shopUI.SetActive(false);
            }

            gameOverUI.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
            GameObject ch = GameObject.FindGameObjectWithTag("Crosshair");
            if (ch && ch.activeInHierarchy) {
                ch.SetActive(false);
            }
        }



    }

    private void SetPlayerFundsTxt(int _funds) {
        txtFunds.text = "$" + _funds.ToString();
    }

    public void SetPlayerHealthBar(float _amount) {
        playerHealthBarFill.localScale = new Vector3(_amount, 1f, 1f);

    }

    private void SetBaseHealthBar(float _amount) {

        baseHealthBarFill.localScale = new Vector3(_amount, 1f, 1f);
    }

    private void SetPhaseTxt(GameManager.GameState _state, int _countdown) {
        if (_state == GameManager.GameState.PHASE_BUILDING) {
			if (isFirstWave) {
				txtPhase.text = "Press ENTER to begin.";
			} else {
				txtPhase.text = "Building: " + _countdown + "s";
			}
        } else if (_state == GameManager.GameState.PHASE_FIGHTING) {
            txtPhase.text = "Battle!";
        }
    }

    private void SetTowerTxt(int _price, string _type) {
        txtTowerPrice.text = "- $" + _price;
        txtTowerType.text = _type;
    }

	private void HideAllTowers(){
		electric.SetActive (false);
		plasma.SetActive (false);
		trigun.SetActive (false);
	}

	private void ShowTower(GameObject tower){
		HideAllTowers ();
		tower.SetActive (true);
	}
}
