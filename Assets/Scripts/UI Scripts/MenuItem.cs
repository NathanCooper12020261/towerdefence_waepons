﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class MenuItem : MonoBehaviour {

	private Animator animator;
	private AudioSource audioSource;

	public string SceneOnClick;

	void Start(){
		animator = GetComponent<Animator> ();
		animator.SetBool ("MouseOver", false);

		audioSource = GetComponent<AudioSource> ();
	}

	void OnMouseEnter(){
		animator.SetBool ("MouseOver", true);
		audioSource.Play ();
	}

	void OnMouseExit(){
		animator.SetBool ("MouseOver", false);
	}

	void OnMouseDown(){
        SceneManager.LoadScene (SceneOnClick);
	}
}
