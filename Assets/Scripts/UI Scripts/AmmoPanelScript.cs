﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AmmoPanelScript : MonoBehaviour {

	//private Text ammoText;

	public GameObject currentAmmo;
	public GameObject extraAmmo;

	public GameObject rifle, pistol, shotgun, sniper;

	private Text currentAmmoText;
	private Text extraAmmoText;

	// Use this for initialization
	void Start () {
		//ammoText = this.GetComponent<Text> ();	
		currentAmmoText = currentAmmo.GetComponent<Text>();
		extraAmmoText = extraAmmo.GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (currentAmmo != null) {
			currentAmmoText = currentAmmo.GetComponent<Text> ();
		}
		if (extraAmmo != null) {
			extraAmmoText = extraAmmo.GetComponent<Text> ();
		}

		switch (PlayerHelper.GetPlayer ().GetComponent<WeaponManger> ().GetCurrentWeapon ().gunID) {
		case 0:
			{
				HideAll ();
				break;
			}
		case 1:
			{
				HideAll ();
				Show (rifle);
				break;
			}
		case 5:
			{
				HideAll ();
				Show (shotgun);
				break;
			}
		case 3:
			{
				HideAll ();
				Show (pistol);
				break;
			}

		case 4:
			{
				HideAll ();
				Show (sniper);
				break;
			}

		}
	
	}


	private void HideAll(){
		pistol.SetActive (false);

		rifle.SetActive (false);

		sniper.SetActive (false); 

		shotgun.SetActive (false);
	}

	private void Show(GameObject obj){
		obj.SetActive (true);
	}






	public void UpdateAmmoText(int amount, int extra){
		currentAmmoText.text = "" + amount;
		extraAmmoText.text = "" + extra;
	}




	public void UpdateAmmoText(string text, string extraText){
		currentAmmoText.text = text;
		extraAmmoText.text = extraText;
	}

}
