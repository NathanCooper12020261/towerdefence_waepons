﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AmmoItemStoreItem : BuyItem {

	public GameObject weaponWhoseAmmoToBuy;


	// Use this for initialization
	void Start () {
		nameLabel.GetComponent<Text> ().text = itemName;
		priceLabel.GetComponent<Text> ().text = "$" + price;

		GameObject[] gos = GameObject.FindObjectsOfType(typeof(GameObject)) as GameObject[]; //will return an array of all GameObjects in the scene
		foreach(GameObject go in gos)
		{
			if(go.layer == 8 && go.CompareTag("Player"))//go.layer=="LocalPlayer" && go.CompareTag("Player"))
			{
				player = go; 
			}
		} 


		SetButton();
	}
	
	// Update is called once per frame
	void Update () {
		nameLabel.GetComponent<Text> ().text = itemName;
		priceLabel.GetComponent<Text> ().text = "$" + price;

		if (PlayerBought ()) {
			if (PlayerHasFunds ()) {
				buyButton.GetComponent<BuyAmmoItemScript> ().SetCanBuy (true);
				buyButton.GetComponent<BuyAmmoItemScript> ().SetPrice (price);
			} else {
				buyButton.GetComponent<BuyAmmoItemScript> ().SetCanBuy (false);
			}
		}

	}

	void SetButton(){
		Debug.Log ("Entered Shop Item " + itemName);

		//Check if player has enough funds
		buyButton.SetActive (true);	
		if (PlayerBought ()) {
			if (PlayerHasFunds ()) {
				buyButton.GetComponent<BuyAmmoItemScript> ().SetCanBuy (true);
				buyButton.GetComponent<BuyAmmoItemScript> ().SetPrice (price);
			} else {
				buyButton.GetComponent<BuyAmmoItemScript> ().SetCanBuy (false);
			}
		
	
		} else {
			buyButton.GetComponent<BuyAmmoItemScript> ().SetCanBuy (false);
		}
	}

	bool PlayerHasFunds(){
		//GameObject player = GameObject.FindGameObjectWithTag ("Player"); player inhereted
		if (player != null) {
			return player.GetComponent<Player> ().funds >= price;
		} else {
			return false;
		}

	}


	bool PlayerBought(){
		WeaponManger weaponManager = player.GetComponent<WeaponManger>(); //GameObject.FindGameObjectWithTag ("Player").GetComponent<WeaponManger>();
		BaseWeapon weapon = weaponWhoseAmmoToBuy.GetComponent<BaseWeapon>();
		return weaponManager.ContainsWeapon (weapon);
	}



}
