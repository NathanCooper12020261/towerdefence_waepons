﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TutorialUIScript : MonoBehaviour {

	public Text buttonText;
	public Text textGameObject;
	public bool isButtonClicked;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetUIText(string text){
		textGameObject.text = text;
	}

	public void SetUIText(string text, string buttonText){
		textGameObject.text = text;
		SetButtonText (buttonText);
	}

	public void SetButtonText(string text){
		buttonText.text = text;
	}

	public void Click(){
		isButtonClicked = true;
	}
}
