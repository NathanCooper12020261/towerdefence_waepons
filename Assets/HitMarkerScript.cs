﻿using UnityEngine;
using System.Collections;

public class HitMarkerScript : MonoBehaviour {

	public AudioSource source;
	public GameObject hitmarker;
	private float timeToHit;
	private float timeSpent;
	bool isHitting;
	// Use this for initialization
	void Start () {
	
		isHitting = false;
		timeToHit = 0.5f;
	}
	
	// Update is called once per frame
	void Update () {
	
		timeSpent += Time.deltaTime;
		if (timeSpent >= timeToHit) {
			isHitting = false;
			timeSpent = 0;
		}

		if (isHitting) {
			hitmarker.gameObject.SetActive (true);
		} else {
			hitmarker.gameObject.SetActive (false);
		}
	}

	public void Hit(){
		hitmarker.gameObject.SetActive (true);
		isHitting = true;
		source.Play ();
	}
}
