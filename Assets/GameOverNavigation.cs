﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
public class GameOverNavigation : NetworkBehaviour {
    GameObject networkManager;
	// Use this for initialization
	void Start () {
        networkManager = GameObject.FindGameObjectWithTag("NetworkManager");
	}
	
	// Update is called once per frame
	void Update () {
        if (networkManager == null) {
            networkManager = GameObject.FindGameObjectWithTag("NetworkManager");
        }
    }
    public void Restart() {
        string name = SceneManager.GetActiveScene().name;
        networkManager.GetComponent<NetworkManager>().StopHost();
        Destroy(networkManager);
        SceneManager.LoadScene(name);
    }

	
    public void GoToMain() {
        networkManager.GetComponent<NetworkManager>().StopHost();
        Destroy(networkManager);

        SceneManager.LoadScene("MainMenu");
    }
   
}
