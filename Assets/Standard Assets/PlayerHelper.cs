﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public static class PlayerHelper{
	public static GameObject GetPlayer(){
		GameObject[] gos = GameObject.FindObjectsOfType(typeof(GameObject)) as GameObject[]; //will return an array of all GameObjects in the scene
		foreach(GameObject go in gos)
		{
			if (go.layer == 8 && go.CompareTag ("Player")) {//go.layer=="LocalPlayer" && go.CompareTag("Player"))
				return go;
			}
		} 
		return null;
	}
}
